#include "turnmanager.h"

#include "battlecharactermanager.h"
#include "playermanager.h"
#include "foemanager.h"
#include "console.h"
#include "player.h"
#include "foe.h"

TurnManager::TurnManager()
    : mCurrentTurnNumber(1)
{

}

void TurnManager::nextTurn(PlayerManager *playerManager, FoeManager *foeManager)
{
    displayTurnInfo();

    playerManager->character()->move(foeManager);
    foeManager->character()->move(playerManager);

    ++mCurrentTurnNumber;
}

void TurnManager::displayTurnInfo()
{
    Console::displayInt(mCurrentTurnNumber);
    Console::displayMessageWithLineEnding(" turn begins");
}

bool TurnManager::hasTheLastTurnPassed() const
{
    bool hasLastTurnPassed = (mCurrentTurnNumber == TURN_LIMIT+1);
    return hasLastTurnPassed;
}
