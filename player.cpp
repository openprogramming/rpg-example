#include "player.h"

#include "console.h"
#include "battlecharactermanager.h"

Player::Player()
{
}

Player::~Player()
{
}

void Player::move(BattleCharacterManager *battleCharacterManager)
{
    if (isDead())
        return;

    BattleCharacter::move(battleCharacterManager);

    displayMoveInfo();
    readInput();
    attackOrDefend(battleCharacterManager);
}

void Player::displayMoveInfo()
{
    Console::displayMessageWithLineEnding("What move do you want to make?");
    Console::displayLineEnding();
    Console::displayMessageWithLineEnding("1: Attack!");
    Console::displayMessageWithLineEnding("2: Defend.");
}

void Player::readInput()
{
    mIntValue = Console::readInt();
}

void Player::attackOrDefend(BattleCharacterManager *battleCharacterManager)
{
    if (mIntValue == 1)
        attack(battleCharacterManager->character());
    else if(mIntValue == 2)
        defend();
    else
    {
        Console::displayLineEnding();
        Console::displayMessageWithLineEnding("Sorry, wrong input.");
    }
}
