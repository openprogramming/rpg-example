#include "playermanager.h"

#include "playercreator.h"
#include "player.h"

PlayerManager::PlayerManager()
    : mPlayer(0)
{
}

PlayerManager::~PlayerManager()
{
    if (mPlayer)
        delete mPlayer;
}

void PlayerManager::createCharacter()
{
    PlayerCreator playerCreator;
    mPlayer = playerCreator.create();
}

BattleCharacter *PlayerManager::character()
{
    return mPlayer;
}
