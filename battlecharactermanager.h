#ifndef BATTLECHARACTERMANAGER_H
#define BATTLECHARACTERMANAGER_H

class BattleCharacter;

class BattleCharacterManager
{
public:
    virtual ~BattleCharacterManager() {}

    virtual void createCharacter() = 0;
    virtual BattleCharacter *character() = 0;
};

#endif // BATTLECHARACTERMANAGER_H
