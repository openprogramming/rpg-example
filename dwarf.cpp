#include "dwarf.h"

#include "console.h"
#include "foe.h"

Dwarf::Dwarf()
{
    mLifePoints = 160;
    mAttack = 50;
    mDefend = 50;

    Console::displayMessageWithLineEnding("I'm a Dwarf!");
}

Dwarf::~Dwarf()
{
}
