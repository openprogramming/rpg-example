#include "battlecharacter.h"

#include "console.h"

BattleCharacter::BattleCharacter()
    : mLifePoints(0)
    , mAttack(0)
    , mDefend(0)
    , mIsDefending(false)
{
}

BattleCharacter::~BattleCharacter()
{
}

void BattleCharacter::defend()
{
    mIsDefending = true;
    Console::displayMessage(name());
    Console::displayMessageWithLineEnding(" is defending");
}

void BattleCharacter::attack(BattleCharacter *enemy)
{
    double multiplier = 1.0;
    if (enemy->isDefending())
        multiplier = 0.5;

    int damage = (int) multiplier * mAttack;
    enemy->hit(damage);
}

void BattleCharacter::hit(int damage)
{
    recalculateLifePoints(damage);
    displayLifePointsInfo(damage);
}

void BattleCharacter::recalculateLifePoints(int damage)
{
    if (damage > mLifePoints)
        mLifePoints = 0;
    else
        mLifePoints -= damage;
}

void BattleCharacter::displayLifePointsInfo(int damage)
{
    Console::displayMessage(name());
    Console::displayMessage(" got hit of ");
    Console::displayInt(damage);
    Console::displayMessage(" points and it's now ");
    Console::displayInt(mLifePoints);
    Console::displayMessageWithLineEnding(" points left.");

    if (isDead())
    {
        Console::displayMessage(name());
        Console::displayMessageWithLineEnding(" is dead!");
    }
}

void BattleCharacter::move(BattleCharacterManager *)
{
    mIsDefending = false;
}
