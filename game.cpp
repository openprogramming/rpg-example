#include <cstdlib>
#include <ctime>
#include "game.h"

#include "playermanager.h"
#include "turnmanager.h"
#include "foemanager.h"
#include "console.h"
#include "player.h"
#include "foe.h"

Game::Game()
    : mPlayerManager(new PlayerManager())
    , mTurnManager(new TurnManager())
    , mFoeManager(new FoeManager)
{
}

Game::~Game()
{
    delete mFoeManager;
    delete mTurnManager;
    delete mPlayerManager;
}

void Game::init()
{
    srand(time(0));

    Console::displayMessageWithLineEnding("Welcome to this superb game!");
    mPlayerManager->createCharacter();
    mFoeManager->createCharacter();
}

void Game::start()
{
    Console::displayMessageWithLineEnding("The game has started.");

    while( !isOver() )
        mTurnManager->nextTurn(mPlayerManager, mFoeManager);

    Console::displayMessageWithLineEnding("The game is over.");

    if (mPlayerManager->character()->isAlive())
        Console::displayMessageWithLineEnding("You won!!!");
    else
        Console::displayMessageWithLineEnding("You lost. :/");

    Console::displayLineEnding();

}

bool Game::isOver() const
{
    bool isPlayerDead = mPlayerManager->character()->isDead();
    bool isFoeDead = mFoeManager->character()->isDead();
    bool hasTheLastTurnPassed = mTurnManager->hasTheLastTurnPassed();

    bool isGameOver = isPlayerDead || isFoeDead || hasTheLastTurnPassed;
    return isGameOver;
}
