#ifndef PLAYERMANAGER_H
#define PLAYERMANAGER_H

#include "battlecharactermanager.h"

class Player;

class PlayerManager : public BattleCharacterManager
{
public:
    PlayerManager();
    virtual ~PlayerManager();

    virtual void createCharacter();
    virtual BattleCharacter *character();

private:
    Player *mPlayer;
};

#endif // PLAYERMANAGER_H
