#ifndef FOEMANAGER_H
#define FOEMANAGER_H

#include "battlecharactermanager.h"

class Foe;

class FoeManager : public BattleCharacterManager
{
public:
    FoeManager();
    virtual ~FoeManager();

    virtual void createCharacter();
    virtual BattleCharacter *character();

private:
    Foe *mFoe;
};

#endif // FOEMANAGER_H
