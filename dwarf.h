#ifndef DWARF_H
#define DWARF_H

#include "player.h"

class Dwarf : public Player
{
public:
    Dwarf();
    virtual ~Dwarf();

    virtual const String name() const
    { return "Dwarf"; }
};

#endif // DWARF_H
