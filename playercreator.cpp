#include "playercreator.h"

#include "console.h"
#include "player.h"
#include "elf.h"
#include "dwarf.h"
#include "defines.h"

PlayerCreator::PlayerCreator()
    : mIntValue(0)
{
}

Player* PlayerCreator::create()
{
    displayCreationInfo();
    readInput();
    Player *newPlayer = createProperCharacter();
    return newPlayer;
}

void PlayerCreator::displayCreationInfo()
{
    Console::displayMessageWithLineEnding("Which character do you want to be?");
    Console::displayLineEnding();
    Console::displayMessageWithLineEnding("1: An Elf");
    Console::displayMessageWithLineEnding("2: A Dwarf");
}

void PlayerCreator::readInput()
{
    mIntValue = Console::readInt();
}

Player *PlayerCreator::createProperCharacter()
{
    Player *newPlayer = 0;
    if (mIntValue == 1)
        newPlayer = createElf();
    else if(mIntValue == 2)
        newPlayer = createDwarf();
    else
    {
        Console::displayLineEnding();
        Console::displayMessageWithLineEnding("Sorry, wrong input.");
    }

    return newPlayer;
}

Player *PlayerCreator::createElf()
{
    Player *player = new Elf();
    return player;

}

Player *PlayerCreator::createDwarf()
{
    Player *player = new Dwarf();
    return player;
}
