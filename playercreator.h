#ifndef PLAYERCREATOR_H
#define PLAYERCREATOR_H

class Player;

class PlayerCreator
{
public:
    PlayerCreator();

    Player *create();

private:
    void displayCreationInfo();
    void readInput();
    Player *createProperCharacter();

    Player *createElf();
    Player *createDwarf();

    int mIntValue;
};

#endif // PLAYERCREATOR_H
